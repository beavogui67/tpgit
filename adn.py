#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Sun Jan 10 12:13:29 2021

@author: angelina beavogui

TP Introduction GIT

"""

import re

def is_valid(adn):
    return bool(re.search(r"^[atgc]+$", adn))
 
def get_vaild_adn() :
    while not is_valid(input("Entrer une chaîne ADN :")) :
        print("La chaîne d'ADN entrer n'est pas valide ! \n Entrer une nouvelle chaîne SVP !")
    print("La chaîne d'ADN entrer est valide !")
